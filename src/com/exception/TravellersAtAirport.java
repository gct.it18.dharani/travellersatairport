package com.exception;

import java.text.DecimalFormat;

public class TravellersAtAirport {
    private String travellerName;
    private double luggageWeight;

    TravellersAtAirport(String travellerName, double luggageWeight) {
        this.travellerName = travellerName;
        this.luggageWeight = luggageWeight;
    }

    public void setTravellerName(String travellerName) {
        this.travellerName = travellerName;
    }

    public void setLuggageWeight(double luggageWeight) {
        this.luggageWeight = luggageWeight;
    }

    public String getTravellerName() {
        return travellerName;
    }

    public double getLuggageWeight() {
        return luggageWeight;
    }

    private void calculateAdditionalCharge() {
        double allowedWeight = 15.0;
        double extraWeight = this.getLuggageWeight() - allowedWeight;

        try {
            if(extraWeight <= 0){
                System.out.println("Additional Charge not required");
            }
            else  if ( extraWeight <= 0.2){
                throw new Exception();
            }
            else {
                double extraCostPerKg = 500;
                double additionalChargeToPay = 0;
                additionalChargeToPay =  extraWeight * extraCostPerKg;
                System.out.println(this.getTravellerName() + " have to pay additional charge of " + additionalChargeToPay);
            }
        } catch (Exception e) {
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            System.out.println("My Extra Weight is just " + decimalFormat.format(extraWeight) +
                    ", please allow me without additional Charges");
           // e.printStackTrace();
        }
        finally {
            System.out.println("Happy Journey! " + this.getTravellerName());
        }
    }

    public static void main(String[] args) {
        TravellersAtAirport bhavya = new TravellersAtAirport("Bhavya", 15.1);
        TravellersAtAirport shruti = new TravellersAtAirport("Shruti", 18);

        bhavya.calculateAdditionalCharge();
        shruti.calculateAdditionalCharge();
    }
}